## Todo-API
Todo API implemented using NodeJs

---

## Useful libraries
### Express  
It helps to write handlers for request:s with different HTTP verbs at different URL paths (routes).

### Joi
It helps with data validation for JavaScript.

---

## Developer Environment
- NVM v0.37.2
  - Node v14.17.0 (https://nodejs.org/en/download/)
- DIRENV v2.27.0
- NPM v6.14.13

---

## How to run
### Clone the project
`$ git clone https://gitlab.com/nataliia.ishchenko/todo-api`

### Install all dependencies
`$ npm install`

### Configure port
By default server will start on port 5000 but it can be changed by setting env variable:
`$ export TODOPORT=8080`

### Run
`$ node app.js`

---

## How to use the API
### Listing existing todos  
* Request: `GET /api/todos`
* Response example:
```json
[
    {
        "id": 1,
        "name": "Have lunch",
        "completed": true
    },
    {
        "id": 3,
        "name": "Pick up son",
        "completed": false
    }
]
```

### Creating a new todo item
* Request: `POST /api/todos`
* Request body:
```json
{
    "name": "Pick up son"
}
```
* Response example:
```json
[
    {
        "id": 3,
        "name": "Pick up son",
        "completed": false
    }
]
```

### Editing a todo item
* Request: `PUT /api/todos/:id`
* Request body:
```json
{
    "name": "Have lunch"
}
```
* Response example:
```json
[
    {
        "id": 1,
        "name": "Have lunch",
        "completed": false
    }
]
```
### Deleting a todo item
* Request: `DELETE /api/todos/:id`
* Response example
```json
[
    {
        "id": 2,
        "name": "todo2",
        "completed": false
    }
]
```
### Marking a todo item as complete
* Request: `PUT /api/todos/:id/completed`
* Response example
```json
[
    {
        "id": 1,
        "name": "Have lunch",
        "completed": true
    }
]
```

Аor simplicity, all results has been saved to json file (data/todos.json). For a real project, it is better to implement this using NoSQL db, for example.

## Testing

The code has been tested with Postman. You can find [the postman collection here](postman/Todos.postman_collection.json).