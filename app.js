const Joi  = require('joi');
const express = require('express');
const fs = require('fs');

const app = express();

app.use(express.json()); 

const todos = require('./data/todos.json');

const path = './data/todos.json';

app.get('/', (req, res) => {
    res.send("Todo App");
});

// Listing existing todos
app.get('/api/todos', (req, res) => {
    res.send(todos);
});

// Creating a new todo item
app.post('/api/todos', (req, res) => {
    console.log(req.body);
    const { error } = validateTodo(req.body);
    if (error) return res.status(400).send(error.detailes[0].message);
    const todo = {
        id: todos.length + 1,
        name: req.body.name,
        completed: false
    };
    todos.push(todo);
    write(todos, path);
    res.send(todo);
});

// Editing a todo item
app.put('/api/todos/:id', (req, res) => {
    const todo = todos.find(t => t.id === parseInt(req.params.id));
    if (!todo) return res.status(404).send('Todo with the given ID not found');

    const { error } = validateTodo(req.body);
    if (error) return res.status(400).send(error.detailes[0].message);
    
    todo.name = req.body.name;
    write(todos, path);
    res.send(todo);
});

// Deleting a todo item
app.delete('/api/todos/:id', (req, res)=>{
    const todo = todos.find(t => t.id === parseInt(req.params.id));
    if (!todo) return res.status(404).send('Todo with the given ID not found');

    //delete
    const index = todos.indexOf(todo);
    todos.splice(index, 1);
    write(todos, path);
    res.send(todo);
})

//  Marking a todo item as completed
app.put('/api/todos/:id/complete', (req, res) => {
    const todo = todos.find(t => t.id === parseInt(req.params.id));
    if (!todo) return res.status(404).send('Todo with the given ID not found');

    todo.completed = true;
    write(todos, path);
    res.send(todo);
});

function validateTodo(todo){
    const schema = Joi.object({
        name: Joi.string().min(1).required()
    });
    return schema.validate(todo);
}

function write(array, path) {
    fs.writeFileSync(path, JSON.stringify(array));
}

const port = process.env.TODOPORT || 5000;
app.listen(port, () => console.log(`Todo App is running on port ${port}`));
